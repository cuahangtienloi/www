---
title: "Nước xả Đại dương"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "bịch 2.1 lít"

# product Price
price: "122.000"
priceBefore: "132.000"

# Product Short Description
shortDescription: "Số lượng ít"

#product ID
productID: "10"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/kirumi/nuoc-xa-dai-duong/nuoc-xa-dai-duong1.png"
  - image: "images/products/kirumi/nuoc-xa-dai-duong/nuoc-xa-dai-duong2.png"
  - image: "images/products/kirumi/nuoc-xa-dai-duong/nuoc-xa-dai-duong3.png"
  - image: "images/products/kirumi/nuoc-xa-dai-duong/nuoc-xa-dai-duong5.png"
---

Giá ưu đãi

Hạn sử dụng mới

Hàng chính hãng 100%