---
title: "Giò lụa tai heo"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "1 kí"

# product Price
price: "280.000"
priceBefore: "300.000"

# Product Short Description
shortDescription: "Quy cách: gói lá hoàn toàn 0.5 kí / đòn"

#product ID
productID: "10"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/kirumi/gio-lua-tai-heo/gio-lua-tai-heo1.png"
  - image: "images/products/kirumi/gio-lua-tai-heo/gio-lua-tai-heo2.png"
  - image: "images/products/kirumi/gio-lua-tai-heo/gio-lua-tai-heo3.png"
  - image: "images/products/kirumi/gio-lua-tai-heo/gio-lua-tai-heo5.png"
---

Đây là sản phẩm do cô người Huế đang sống tại Qui Nhơn tận tay làm, chăm chút và gói ghém mang lên.

Sản phẩm có đầy đủ giấy tờ chứng nhận VSATTP và đăng kí kinh doanh. Ngoài ra, sản phẩm bên mình còn được đặt riêng với những tiêu chuẩn cao hơn thông thường vì vậy, giá thành cũng tương xứng với giá trị sản phẩm. 

Mỗi sản phẩm từ bếp đều được làm như làm cho nhà ăn. Đặc biệt, hàng tươi mới lên mỗi tuần và được bảo quản ngăn mát. 