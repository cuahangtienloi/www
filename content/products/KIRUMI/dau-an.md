---
title: "Dàu ăn "
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "1 lít  "

# product Price
price: "26.000"
priceBefore: "36.700"

# Product Short Description
shortDescription: " Dầu ăn cao cấp Gold Tường An "

#product ID
productID: "12"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/kirumi/dau-an/dau-an1.png"
  - image: "images/products/kirumi/dau-an/dau-an2.png"
  - image: "images/products/kirumi/dau-an/dau-an3.png"
  - image: "images/products/kirumi/dau-an/dau-an5.png"
---

Hàng trợ giá 

Số lượng rất ít 

Hạn sử dụng mới