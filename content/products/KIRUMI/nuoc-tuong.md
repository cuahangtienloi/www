---
title: "Nước tương"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "chai 700ml"

# product Price
price: "21.000"
priceBefore: "31.800"

# Product Short Description
shortDescription: "Số lượng ít"

#product ID
productID: "10"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/kirumi/nuoc-tuong/nuoc-tuong1.png"
  - image: "images/products/kirumi/nuoc-tuong/nuoc-tuong2.png"
  - image: "images/products/kirumi/nuoc-tuong/nuoc-tuong3.png"
  - image: "images/products/kirumi/nuoc-tuong/nuoc-tuong5.png"
---

Giá ưu đãi

Hạn sử dụng mới

Hàng chính hãng 100%