---
title: "Bò viên"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "1 kí"

# product Price
price: "350.000"
priceBefore: "370.000"

# Product Short Description
shortDescription: "Quy cách: đóng gói hút chân không 0.5 kí"

#product ID
productID: "7"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/kirumi/bo-vien/bo-vien1.png"
  - image: "images/products/kirumi/bo-vien/bo-vien2.png"
  - image: "images/products/kirumi/bo-vien/bo-vien3.png"
  - image: "images/products/kirumi/bo-vien/bo-vien5.png"
---

Đây là sản phẩm do cô người Huế đang sống tại Qui Nhơn tận tay làm, chăm chút và gói ghém mang lên.

Sản phẩm có đầy đủ giấy tờ chứng nhận VSATTP và đăng kí kinh doanh. Ngoài ra, sản phẩm bên mình còn được đặt riêng với những tiêu chuẩn cao hơn thông thường vì vậy, giá thành cũng tương xứng với giá trị sản phẩm. 

Mỗi sản phẩm từ bếp đều được làm như làm cho nhà ăn. Đặc biệt, hàng tươi mới lên mỗi tuần và được bảo quản ngăn mát. 