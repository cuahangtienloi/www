---
title: "Nước xả Sakura"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "bịch 1.5 lít"

# product Price
price: "69.000"
priceBefore: "99.000"

# Product Short Description
shortDescription: "Số lượng ít"

#product ID
productID: "10"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/kirumi/nuoc-xa-sakura/nuoc-xa-sakura1.png"
  - image: "images/products/kirumi/nuoc-xa-sakura/nuoc-xa-sakura2.png"
  - image: "images/products/kirumi/nuoc-xa-sakura/nuoc-xa-sakura3.png"
  - image: "images/products/kirumi/nuoc-xa-sakura/nuoc-xa-sakura5.png"
---

Giá ưu đãi

Hạn sử dụng mới

Hàng chính hãng 100%