---
title: "Thuốc nhuộm Loreal Đức"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Hộp"

# product Price
price: "295.000"
priceBefore: "350.000"

# Product Short Description
shortDescription: "Hàng Đức"

#product ID
productID: "4"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/northen-fields/loreal-duc/loreal-duc1.png"
  - image: "images/products/northen-fields/loreal-duc/loreal-duc2.png"
  - image: "images/products/northen-fields/loreal-duc/loreal-duc3.png"
  - image: "images/products/northen-fields/loreal-duc/loreal-duc5.png"
---

Có 2 màu cho bạn chọn lựa : Màu nâu trung tính và nâu vàng

Số lượng có hạn