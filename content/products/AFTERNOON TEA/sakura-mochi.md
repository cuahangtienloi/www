---
title: "Sakura-mochi "
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "100gr"

# product Price
price: "20.000"
priceBefore: "25.000"

# Product Short Description
shortDescription: " hàng order trước ít nhất 1 ngày "

#product ID
productID: "16"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/afternoon-tea/egg-tart/eggtart1.png"
  - image: "images/products/afternoon-tea/egg-tart/eggtart1.png"
  - image: "images/products/afternoon-tea/egg-tart/eggtart1.png"
  - image: "images/products/afternoon-tea/egg-tart/eggtart1.png"
---

Công thức bao gồm: thạch agar Nhật, nước suối, đường, hoa Sakura muối, đậu nành rang và nước đường nâu. 

Hàng chỉ được sau khi nhận được đơn hàng 

Bánh dùng lạnh