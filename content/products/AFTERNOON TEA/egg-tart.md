---
title: "Bánh trứng Original "
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "hàng order trước ít nhất 3 tiếng"

# product Price
price: "10.000"
priceBefore: "12.000"

# Product Short Description
shortDescription: " bánh được làm theo công thức nguyên bản từ Bồ Đào Nha và điều chỉnh độ ngọt, hương vị cho phù hợp hơn với khẩu vị người Việt đại "

#product ID
productID: "15"

# type must be "products" 
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/afternoon-tea/egg-tart/eggtart1.png"
  - image: "images/products/afternoon-tea/egg-tart/eggtart2.png"
  - image: "images/products/afternoon-tea/egg-tart/eggtart3.png"
  - image: "images/products/afternoon-tea/egg-tart/eggtart5.png"
---

Là chiếc bánh ấm nóng được bao bọc bởi ngàn lớp vỏ giòn xốp nhưng không ngấy dầu, bên trong là lớp bánh thơm nhẹ mùi trứng, mùi sữa cùng hòa quyện với lóp bơ thơm mỏng ngoài vỏ. 

1 set 6 cái : 72.000 / 1 set 12 cái : 130.000 / 1 set 30 cái : 300.000 

Bánh được nướng mới mỗi đơn hàng và ngon nhất khi dùng ấm.