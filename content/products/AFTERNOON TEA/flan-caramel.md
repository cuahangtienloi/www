---
title: "Flan Caramel"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description : "Hàng order trước 1 ngày"
# product Price
price: "10.000"
priceBefore: "12.000"

# Product Short Description
shortDescription: " hũ nhựa "

#product ID
productID: "17"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/products/afternoon-tea/egg-tart/eggtart1.png"
  - image: "images/products/afternoon-tea/egg-tart/eggtart1.png"
  - image: "images/products/afternoon-tea/egg-tart/eggtart1.png"
  - image: "images/products/afternoon-tea/egg-tart/eggtart1.png"
---

Bánh được làm theo công thức riêng của bếp 

Thành phần gồm lớp caramel thơm nhẹ tự, trứng ,sữa tươi, sữa đặc. Có thể điều chỉnh độ ngọt theo yêu cầu.
Bên mình làm ngọt bằng sữa đặc thay cho đường nên các bạn cân nhắc khi muốn giảm ngọt nhé.  
