---
title: "Kirumi"
date: 2019-10-17T11:22:16+06:00
draft: false
description : "Mỗi thực khách là người bạn mà gắn bó dài lâu. "
image: "images/blog/post-1.jpg"
author: "Nga Nguyen"
---

Kirumi đầu tiên được xây dựng theo hình ảnh bếp Nhật, phục vụ những món bánh ngọt theo phong cách Nhật, đơn giản, tinh tế và khá kì công , đông thời cũng xây dựng theo tiêu chí vì sức khỏe như sản phẩm luôn tươi mới, các nguyên liệu được xử lý cẩn thận, xuất xứ rõ ràng , an toàn cho sức khỏe. Theo thời gian, những dòng sản phẩm đã ,đang và sẽ luôn luôn được thay đổi, vận động cho phù hợp với thị hiếu của khách mà không còn gói gọn trong khuôn của bánh Nhật nữa, có thể là bánh Âu, bánh Viêt, bánh kết hợp Á ,Âu, Việt ... nhưng tất cả những sự thay đổi đó vẫn luôn bám sát tiêu chí về sản phẩm của bếp bánh. 



Là bếp bánh ngọt nhưng không ngọt. Hiểu được nỗi lo lắng nhất của người " hiện đại " trong mỗi công thức bánh là chất tạo ngọt và phụ gia. Tất cả các chất tạo ngọt dù có nguồn gốc thiên nhiên vẫn không đem lại lợi ích cho sức khỏe mà ngược lại là thủ phạm gây béo phì, làm tăng quá trình lão hóa. Các chất phụ gia khi lạm dụng cũng là gây ra những ảnh hưởng ít nhiều tới sức khỏe. Kirumi đã giảm lượng đường đến mức tối đa có thể và chắc chắn không lạm dụng phụ gia để các thực khách yên tâm hơn khi sử dụng.  

Và vì bánh ngọt ít đường, không chất bảo quản, phụ gia linh tinh nên bánh không thể để lâu được nên Kirumi luôn làm bánh số lượng ít, chủ yếu dựa vào đơn hàng để đảm bảo bánh mới mỗi ngày. 

> Tiêu chí về sản phẩm của Kirumi 

Tìm kiếm nguồn nguyên liệu chất lượng

Quy trình sơ chế chậm, cẩn thận.

Quy trình chế biến ở điều kiện tốt nhất có thể

Bánh đẹp , có chất và lượng đi cùng giá thành phù hợp và chắc chắn là không lãng phí.  