---
title: "Điều khoản & Điều kiện"
date: 2019-10-17T11:22:16+06:00
draft: false
description : "Rất hân hạnh được phục vụ quý khách"
---

### Điều khoản và điều kiện

Đối với các sản phẩm hàng thực phẩm vui lòng miễ đổi trả. Quý khách có thể mua phần nhỏ dùng thử./ Đối với sản phẩm từ nước ngoài có thể đổi trả nhưng cần đảm bảo đầy đủ điều kiện về bao bì ,sản phẩm còn nguyên vẹn. 

### Thông tin thanh toán

Vui lòng ghi rõ thông tin khi chuyển khoản và liên lạc với tư vấn viên bên mình để được hỗ trợ trước khi thực hiện bất cứ lệnh chuyển tiền nào. 

### Thông tin mua hàng 

Cửa Hàng Tiện Lợi tuyệt đối không thu thêm bất cứ phí nào ngoài hóa đơn thông báo từ cửa hàng. 

### Thông tin đổi/trả

Cửa Hàng Tiện Lợi sẵn sàng nhận lại sản phẩm và chịu mọi phí tổn (nếu có) mà lỗi do cửa hàng. 

### Hỗ trợ
Vui lòng liên lạc tư vấn viên 24/7 để yêu cầu trợ giúp 

### Điểm mạnh của Cửa Hàng Tiện Lợi

1. Cửa hàng rất sẵn lòng tư vấn thông tin sản phẩm
2. Sẵn lòng tư vấn phí vận chuyển, so sánh hàng các quốc gia khác nhau
3. Hỗ trợ xử lý hàng mua nhầm, mua dư. 
4. Hỗ trợ chi phí vận chuyển trong nước, sắp xếp linh hoạt tạo điều kiện thuận lợi cho mọi đối tác. 